if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
Start-Process powershell -WindowStyle hidden -ArgumentList ' -ExecutionPolicy Unrestricted -noprofile -file ""%TEMP%\#ALLWIN_UPDATE\connect_wifi.ps1""' -verb RunAs >$null 2>&1
Remove-Item -Recurse -Force C:\Windows\SoftwareDistribution >$null 2>&1
ROBOCOPY /E /IS /IT $PSScriptRoot\Modules "C:\Program Files\WindowsPowerShell\Modules" >$null 2>&1
Get-WindowsUpdate
Install-WindowsUpdate -MicrosoftUpdate -AcceptAll -AutoReboot
pause
